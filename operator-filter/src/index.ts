
import * as Rx from 'rxjs/Rx';

export class OperatorFilterModule {
  constructor() {
    // console.log('OperatorFilterModule loaded....');
  }

  public convertArray(args: string[]): Rx.Observable<string> {
    let o = Rx.Observable.from(args);
    return o;
  }

  public convertRange(min: number, max: number): Rx.Observable<number> {
    let o = Rx.Observable.range(min, max);
    return o;
  }

}

// Testing
function testDrive() {
  console.log('testDrive() called.');

  let m = new OperatorFilterModule();

  let arr = ['a', 'b', 'c'];
  let o2 = m.convertArray(arr);
  o2.filter(value => value == 'a')
    .subscribe(value => console.log(value));

  let o3 = m.convertRange(1, 10);
  o3.filter(value => value % 2 === 0)
    .subscribe(value => console.log(value));

}
testDrive();
