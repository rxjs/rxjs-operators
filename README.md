# RxJS Operator Tutorial


_tbd_


Common operators

* `of()`
* `from()`
* `do()`
* `filter()`
* `map()`
* `reduce()`
* `scan()`
* `skip()`
* `take()`
* `first()`
* `last()`
* `zip()`
* `distnict()`
* `concat()`
* `combine()`
* `combineLatest()`
* `flatten()`
* `flatMap()`
* `merge()`
* `mergeMap()`
* `switch()`
* `switchMap()`
* `repeat()`
* `retry()`
* `delay()`
* `debounceTime()`
* ...




# References

* [RxJS Tutorial](http://reactivex.io/rxjs/manual/tutorial.html)
* [Learn RxJS](https://www.learnrxjs.io/)
* []()
* []()
* []()
* []()
* []()



